<?php
namespace Bleicker\Framework;

/**
 * Class Kernel
 *
 * @package Bleicker\Framework\Factory
 */
interface KernelInterface {

	/**
	 * @return void
	 */
	public function run();
}