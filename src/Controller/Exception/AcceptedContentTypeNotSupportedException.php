<?php

namespace Bleicker\Framework\Controller\Exception;

use Bleicker\Exception\ThrowableException as Exception;

/**
 * Class AcceptedContentTypeNotSupportedException
 *
 * @package Bleicker\Framework\Controller\Exception
 */
class AcceptedContentTypeNotSupportedException extends Exception {

}
